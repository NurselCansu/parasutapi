<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Http;

use App\User;



class ParasutController extends Controller
{
	public function Client(){
		$user = User::find(21);
		$username = $user->api_username;
		$password = $user->api_password;
		$clientId = $user->api_client_id;
		$companyId = $user->api_company_id;
		

		$client_data = [
				"development" => false, //development mode 
				"client_id" => $clientId,
				"username" => $username,
				"password" => $password,
				"redirect_uri" => "urn:ietf:wg:oauth:2.0:oob",
				"company_id" => $companyId,
			];

		try {
			$parasutAuthorization = new \Parasut\API\Authorization($client_data);
		} catch (\Parasut\API\Exception $e) {
			echo "Error code : " . $e->getCode()."<br>";
			echo "Error message : " . $e->getMessage();
			die;
		}

		return $parasutAuthorization;
	}

	public function listAccounts(){
		$accounts = new \Parasut\API\Accounts($this->Client());

		$accountList = $accounts->list_accounts();
		dd($accountList);
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listContacts()
    {
		
		$contacts = new \Parasut\API\Contacts($this->Client());

		//Contact Lists
		$contactList = $contacts->list_contacts();
		

		return json_encode($contactList);

		
    }

    public function showContact($contact_id=null)
    {
		$contacts = new \Parasut\API\Contacts($this->Client());

    	if($contact_id == null){
    		$contact_id = 69748251;
    	}
		
		$showContact = $contacts->show($contact_id);
		

		return json_encode($showContact);
    }

    public function searchContact($name=null, $email=null)
    {

		$contacts = new \Parasut\API\Contacts($this->Client());

		if($name == null){
			$name = "TEST MÜŞTERİ";
		}
		if($email == null){
			$email = "musteri@test.com";
		}
    	$searchContactData1 = [
			"name" => $name
		];

		$searchContactData2 = [
			"name" => $name,
			"email" => $email
		];

		$searchContactData3 = [
			"name" => $name,
			"email" => $email,
			"tax_number" => "3454231",
			"tax_office" => "Beyoğlu",
			"city" => "İSTANBUL",
			"account_type" => "customer" //customer, supplier
		];
		$searchContact1 = $contacts->search($searchContactData1);

		$searchContact2 = $contacts->search($searchContactData2);
		// dd($searchContact2);

		$searchContact3 = $contacts->search($searchContactData3);
		// dd($searchContact3);

		return json_encode($searchContact1);
    }

    public function createContact(){
    	
		$contacts = new \Parasut\API\Contacts($this->Client());

		//create contact
		$createContactData = [
			"data" => [
				"type" => "contacts",
				"attributes" => [
					"name" => "TEST MÜŞTERİ", //*required //ad soyad
					"email" => "musteri@test.com", //e-posta
					"contact_type" => "person", //company, person (tüzel kişi, gerçek kişi)
					"tax_office" => "Beyoğlu", //vergi dairesi
					"tax_number" => "3454231", //vergi numarası
					"district" => "XXX", //ilçe
					"city" => "İSTANBUL", //il
					"address" => "XXX", //adres
					"phone" => "05555555555", //tel no
					"account_type" => "customer" //customer, supplier
				]
			]
		];


		if(!empty(json_decode($this->searchContact())->result->data)){
			echo 'Contact found';
			// dd(($searchContact3));
		}else{
			echo 'Contact not found';
			$createContact = $contacts->create($createContactData);
			// $searchContact3 = $contacts->search($searchContactData3);

			return json_encode($createContact);
			// dd(($searchContact3));
		}
    }

    public function updateContact($contact_id=null){

    	$contacts = new \Parasut\API\Contacts($this->Client());

		//edit contact
		if($contact_id == null){

			$contact_id = json_decode($this->searchContact())->result->data[0]->id; //integer
		}
		echo 'Contact Id-> '.$contact_id;
		$editContactData = [
			"data" => [
				"type" => "contacts",
				"attributes" => [
					"name" => "TEST MÜŞTERİ", //*required //ad soyad
					"email" => "musteri@test.com", //e-posta
					"contact_type" => "person", //company, person (tüzel kişi, gerçek kişi)
					"tax_office" => "Beyoğlu", //vergi dairesi
					"tax_number" => "3454231", //vergi numarası
					"district" => "XXX", //ilçe
					"city" => "İSTANBUL", //il
					"address" => "XXX", //adres
					"phone" => "05554445454", //tel no
					"account_type" => "customer" //customer, supplier
				]
			]
		];
		if($contact_id != null){
			$editContact = $contacts->edit($contact_id, $editContactData);
		}
		//edit contact

		return json_decode($editContact);
		
    }

    public function deleteContact($contact_id=null){
    	$contacts = new \Parasut\API\Contacts($this->Client());
    	//delete contact
		if(!empty(json_decode($this->searchContact())->result->data)){
			$contact_id = json_decode($this->searchContact())->result->data[0]->id; //integer
			$deleteContact = $contacts->delete($contact_id);
		}
		//delete contact
    }


    public function listProducts(){

    	$products = new \Parasut\API\Products($this->Client());

		//product list
		$productList = $products->list_products();

		return json_encode($productList);
    }

    public function showProduct($product_id = null){

    	$products = new \Parasut\API\Products($this->Client());

    	if($product_id == null){
    		$product_id = 61927512;
    	}

		//product list
		$showProduct = $products->show($product_id);

		return json_encode($showProduct);
    }

    public function searchProduct($name = null, $code=null){

    	$products = new \Parasut\API\Products($this->Client());

    	if($name == null){
    		$name = "Test Ürün";
    	}
    	if($code == null){
    		$code = "XXX";
    	}
    	//search product
		$searchProductData1 = [
			"name" => $name
		];

		$searchProductData2 = [
			"name" => $name,
			"code" => $code
		];

		$searchProduct1 = $products->search($searchProductData1);
		$searchProduct2 = $products->search($searchProductData2);

		return json_encode($searchProduct1);
    }

    public function createProduct(){

    	$products = new \Parasut\API\Products($this->Client());

    	//create contact
		$productData = [
			"data" => [
				"type" => "products",
				"attributes" => [
					"name" => "Test Ürün", //ürün adı
					"vat_rate" => 18, //KDV oranı
					"unit" => "Adet", //birim
					"currency" => "TRL", //döviz tipi
					"inventory_tracking" => true, //stok durumu
					"initial_stock_count" => 1 //stok adedi
				]
			]
		];

		$createProduct = $products->create($productData);

		return json_encode($createProduct);
    }

    public function updateProduct($product_id = null){
    	$products = new \Parasut\API\Products($this->Client());

    	$productData = [
			"data" => [
				"type" => "products",
				"attributes" => [
					"name" => "XXXX", //ürün adı
					"vat_rate" => 18, //KDV oranı
					"unit" => "Adet", //birim
					"currency" => "TRL", //döviz tipi
					"inventory_tracking" => true, //stok durumu
					"initial_stock_count" => 100 //stok adedi
				]
			]
		];

		// $product_id = 123456;
		if($product_id != null){
			
			$editProduct = $products->edit($product_id, $productData);
		}

		return json_encode($editProduct);

    }

    public function deleteProduct($product_id = null){
    	$products = new \Parasut\API\Products($this->Client());

    	if($product_id != null){

			$deleteProduct = $products->delete($product_id);
    	}

    }


    public function listSalesInvoices(){
    	
    	$invoices = new \Parasut\API\Invoices($this->Client());
    	$invoiceList = $invoices->list_invoices();

    	return json_encode($invoiceList);

    }

    public function showSalesInvoice($invoice_id = null){

    	$invoices = new \Parasut\API\Invoices($this->Client());
    	if($invoice_id == null){

    		$invoice_id = 89014876; //integer
    	}
		$showInvoice = $invoices->show($invoice_id); //active_e_document,contact parametreleri ile beraber gelir

		return json_encode($showInvoice);

    }

    public function searchSalesInvoice($invoice_id = null){

    	$invoices = new \Parasut\API\Invoices($this->Client());
    	if($invoice_id == null){
			$invoice_id = 1;
    	}
    	$searchInvoiceData1 = [
			"invoice_id" => $invoice_id
		];

		$searchInvoiceData2 = [
			"invoice_id" => $invoice_id,
			"contact_id" => "69748251",
		];

		$searchInvoiceData3 = [
			"issue_date" => "2020-02-01",
			"due_date" => "2020-04-01",
			"contact_id" => "21447041",
			"invoice_id" => $invoice_id,
			"invoice_series" => "XXX 1",
			"item_type" => "invoice",
			"print_status" => "printed", //printed, not_printed, invoices_not_sent, e_invoice_sent, e_archive_sent, e_smm_sent
			"payment_status" => "paid" //overdue, not_due, unscheduled, paid
		];
		$searchInvoice1 = $invoices->search($searchInvoiceData1);

		$searchInvoice2 = $invoices->search($searchInvoiceData2);
		$searchInvoice3 = $invoices->search($searchInvoiceData3);
		//search invoice

		return json_encode($searchInvoice2);
    }


    public function createSalesInvoice($contact_id=null, $product_id=null){

    	$invoices = new \Parasut\API\Invoices($this->Client());

    	if($contact_id == null){

    		$contact_id = 69748251;
    	}
    	if($product_id == null){

    		$product_id = 61927512;
    	}
    	//create invoice
    	//item_type = "export" -> İhracat Faturaları için ürünlerin GTIP kodu doldurulmalı.

		$createInvoiceData = [
			"data" => [
				"type" => "sales_invoices",
				"attributes" => [
					"item_type" => "invoice", //export
					"description" => "Test açıklama", //fatura açıklaması
					"issue_date" => "2021-11-13", //düzenleme tarihi
					"due_date" => "2021-11-15", //son tahsilat tarihi
					"invoice_series" => "XXX", //fatura seri no
					"invoice_id" => "3", //fatura sıra no
					"currency" => "TRL", //döviz tipi // TRL, USD, EUR, GBP
					"shipment_included" => true, //irsaliye
					"is_abroad" => true,
					"city" => "Ohio",
					"country" => "ABD",
				],
				"relationships" => [
					"details" => [
						"data" => [
							0 => [
								"type" => "sales_invoice_details",
								"attributes" => [
									"quantity" => 1, //birim adedi
									"unit_price" => 100, //birim fiyatı (kdv'siz fiyatı)
									"vat_rate" => 18, //kdv oranı
									"description" => "XXXXX", //ürün açıklaması
									"delivery_method" => "CIF",
									"shipping_method" => "Demiryolu"

								],
								"relationships" => [
									"product" => [
										"data" => [
											"id" => $product_id, //ürün id
											"type" => "products"
										]
									]
								]
							],
		//					1 => [
		//						"type" => "sales_invoice_details",
		//						"attributes" => [
		//							"quantity" => 1, //birim adedi
		//							"unit_price" => 100, //birim fiyatı (kdv'siz fiyatı)
		//							"vat_rate" => 18, //kdv oranı
		//							"description" => "XXXXX" //ürün açıklaması
		//						],
		//						"relationships" => [
		//							"product" => [
		//								"data" => [
		//									"id" => 123456, //ürün id
		//									"type" => "products"
		//								]
		//							]
		//						]
		//					]
						]
					],
					"contact" => [
						"data" => [
							"type" => "contacts",
							"id" => $contact_id //müşteri id
						]
					]
				]
			]
		];
		$createInvoice = $invoices->create($createInvoiceData);
		//create invoice

		return json_encode($createInvoice);
    }


    public function updateSalesInvoice($invoice_id = null, $contact_id){
    	$invoices = new \Parasut\API\Invoices($this->Client());

    	if($invoice_id == null){
    		$invoice_id = 89021052;
    	}
    	if($contact_id == null){
    		$contact_id = 69748251;
    	}
    	//edit invoice
		$editInvoiceData = [
			"data" => [
				"type" => "sales_invoices",
				"attributes" => [
					"item_type" => "invoice",
					"description" => "Test açıklama edit", //fatura açıklaması
					"issue_date" => "2021-11-13", //düzenleme tarihi
					"due_date" => "2021-11-16", //son tahsilat tarihi
					"invoice_series" => "XXX", //fatura seri no
					"invoice_id" => "3", //fatura sıra no
					"currency" => "TRL", //döviz tipi // TRL, USD, EUR, GBP
					"shipment_included" => true, //irsaliye
					"is_abroad" => true,
					"city" => "Ohio",
					"country" => "ABD",
				],
				"relationships" => [
					"details" => [
						"data" => [
							0 => [
								"type" => "sales_invoice_details",
								"attributes" => [
									"quantity" => 1, //birim adedi
									"unit_price" => 100, //birim fiyatı (kdv'siz fiyatı)
									"vat_rate" => 18, //kdv oranı
									"description" => "XXXXX", //ürün açıklaması
									"delivery_method" => "CIF",
									"shipping_method" => "Demiryolu"
								],
								"relationships" => [
									"product" => [
										"data" => [
											"id" => 61927512, //ürün id
											"type" => "products"
										]
									]
								]
							],
		//					1 => [
		//						"type" => "sales_invoice_details",
		//						"attributes" => [
		//							"quantity" => 1, //birim adedi
		//							"unit_price" => 100, //birim fiyatı (kdv'siz fiyatı)
		//							"vat_rate" => 18, //kdv oranı
		//							"description" => "XXXXX" //ürün açıklaması
		//						],
		//						"relationships" => [
		//							"product" => [
		//								"data" => [
		//									"id" => 123456, //ürün id
		//									"type" => "products"
		//								]
		//							]
		//						]
		//					]
						]
					],
					"contact" => [
						"data" => [
							"type" => "contacts",
							"id" => $contact_id //müşteri id
						]
					]
				]
			]
		];

		
		$editInvoice = $invoices->edit($invoice_id, $editInvoiceData);

		return json_encode($editInvoice);

    }

    public function deleteSalesInvoice($invoice_id = null){
    	$invoices = new \Parasut\API\Invoices($this->Client());
    	if($invoice_id != null){
			$deleteInvoice = $invoices->delete($invoice_id);	
    	}
    }

    public function cancelSalesInvoice($invoice_id = null){
    	$invoices = new \Parasut\API\Invoices($this->Client());
    	if($invoice_id != null){
    		$cancelInvoice = $invoices->cancel($invoice_id);
    	}
    }


    public function paySalesInvoice($invoice_id=null, $account_id=null){
    	$invoices = new \Parasut\API\Invoices($this->Client());

    	$account_id = 484815;
    	$invoice_id = 89021052;
    	$amount = 118;

    	$payInvoiceData = [
			"data" => [
				"type" => "payments",
				"attributes" => [
					"account_id" => $account_id, // Kasa veya Banka id
					"date" => "2021-12-14", //ödeme tarihi
					"amount" => $amount //ödeme tutarı
				]
			]
		];

		$invoices->pay($invoice_id, $payInvoiceData);
    	
    }

    public function createEInvoice($vkn = null){

    	$invoices = new \Parasut\API\Invoices($this->Client());

    	$invoice_id = 89021052;

    	if($vkn == null){
    		$vkn = 17258571466;
    		
    	}
		$checkVKNType = $invoices->check_vkn_type($vkn);

		// dd($checkVKNType);

		$eInvoiceAddress = $checkVKNType->result->data[0]->attributes->e_invoice_address;

		if (empty($checkVKNType->result))
		{
		    $eArchiveData = [
			"data" => [
				"type" => "e_archives",
				"relationships" => [
					"sales_invoice" => [
						"data" => [
							"id" => $invoice_id, //invoice_id
							"type" => "sales_invoices"
						    ]
					    ]
				    ]
			    ]
		    ];
		    $createEArchive = $invoices->create_e_archive($eArchiveData);
		}
		//VKN e-fatura sistemine kayıtlı
		else
		{
		
		    $eInvoiceData = [
			"data" => [
				"type" => "e_invoices",
				"attributes" => [
		        	'scenario' => 'basic', // basic veya commercial (temel veya ticari)
		        	'to' => $eInvoiceAddress
		        ],
				"relationships" => [
					"invoice" => [
						"data" => [
							"id" => $invoice_id, //invoice_id
							"type" => "sales_invoices"
						    ]
					    ]
				    ]
			    ]
		    ];
		    $createEInvoice = $invoices->create_e_invoice($eInvoiceData);
		}

		//
		return json_encode($createEInvoice);


    }

   
    public function listEInvoices(){
    	$invoices = new \Parasut\API\Invoices($this->Client());
    	$einvoicesList = $invoices->list_e_invoices(1,25);

    	// dd($einvoicesList);

    	return json_encode($einvoicesList);
    }

    public function showEInvoice(){
    	$invoices = new \Parasut\API\Invoices($this->Client());
    	//show e invoice
		$e_invoice_id = 17292321; //invoice_id değildir.
		$showEArchive = $invoices->show_e_invoice($e_invoice_id);
		//show e invoice

		return json_encode($showEArchive);
    }


    public function showEInvoicePdf($e_invoice_id = null){
    	$invoices = new \Parasut\API\Invoices($this->Client());

    	if($e_invoice_id == null){

    		$e_invoice_id = 17292321; //invoice_id değildir.
    	}
		$pdfEArchive = $invoices->pdf_e_invoice($e_invoice_id);


		$pdfUrl = $pdfEArchive->result->data->attributes->url;
		// $uploadPath = "data/xxxxx.pdf";
		// $uploadPDF = $invoices->upload_pdf($pdfUrl, $uploadPath);
	

		return \Redirect::to($pdfUrl);
    }

    public function showTrackableJob($trackable_id){
    	$invoices = new \Parasut\API\Invoices($this->Client());

		$checkTrackableJobs = $invoices->trackable_jobs($trackable_id);

		return json_encode($checkTrackableJobs);
    }


}