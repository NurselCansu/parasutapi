<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

//Contact
Route::get('listContacts', 'ParasutController@listContacts');
Route::get('showContact', 'ParasutController@showContact');
Route::get('showContact/{id}', 'ParasutController@showContact');
Route::get('searchContact', 'ParasutController@searchContact');
Route::get('createContact', 'ParasutController@createContact');
Route::get('updateContact', 'ParasutController@updateContact');
Route::get('updateContact/{id}', 'ParasutController@updateContact');
Route::get('deleteContact', 'ParasutController@deleteContact');
Route::get('deleteContact/{id}', 'ParasutController@deleteContact');


//Product
Route::get('listProducts', 'ParasutController@listProducts');
Route::get('showProduct', 'ParasutController@showProduct');
Route::get('searchProduct', 'ParasutController@searchProduct');
Route::get('createProduct', 'ParasutController@createProduct');
Route::get('updateProduct', 'ParasutController@updateProduct');
Route::get('deleteProduct', 'ParasutController@deleteProduct');


//Invoice
Route::get('listSalesInvoices', 'ParasutController@listSalesInvoices');
Route::get('showSalesInvoice', 'ParasutController@showSalesInvoice');
Route::get('showSalesInvoice/{id}', 'ParasutController@showSalesInvoice');
Route::get('searchSalesInvoice', 'ParasutController@searchSalesInvoice');
Route::get('createSalesInvoice', 'ParasutController@createSalesInvoice');
Route::get('updateSalesInvoice', 'ParasutController@updateSalesInvoice');
Route::get('updateSalesInvoice/{id}', 'ParasutController@updateSalesInvoice');
Route::get('deleteSalesInvoice', 'ParasutController@deleteSalesInvoice');
Route::get('deleteSalesInvoice/{id}', 'ParasutController@deleteSalesInvoice');
Route::get('cancelSalesInvoice', 'ParasutController@cancelSalesInvoice');
Route::get('cancelSalesInvoice/{id}', 'ParasutController@cancelSalesInvoice');
Route::get('paySalesInvoice', 'ParasutController@paySalesInvoice');


//E-Invoice
Route::get('createEInvoice', 'ParasutController@createEInvoice');
Route::get('listEInvoices', 'ParasutController@listEInvoices');
Route::get('showEInvoice', 'ParasutController@showEInvoice');
Route::get('showEInvoicePdf', 'ParasutController@showEInvoicePdf');
Route::get('showEInvoicePdf/{id}', 'ParasutController@showEInvoicePdf');
Route::get('showTrackableJob', 'ParasutController@showTrackableJob');


Route::get('listAccounts', 'ParasutController@listAccounts');













